# configuration server files
Microservices
• Web Data Finder Service (WDFS)
• Availability Checker Service (ACS)
• API Manager Service (AMS)
• Dynamic Service Generator Service (DSGS)
• Code Supplier Service (CSS)
• Service Manager Service (SMS)
• Real-time Data Input Service (RTDIS)
• Json Data Ripper Service (JDRS)
• Change Detection Service (CDS)
• Scoring Service (SS)
• Reporting Service (RS)

# You can use any of the profile dev prod and default 
